<script type="text/javascript" language="javascript" src="/resources/js/jquery/jquery.min.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/skinnytip.js"></script>
<script type='text/javascript' language="javascript" src="/resources/js/jsmd5.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/jquery.animate-shadow.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/jquery.ui.widget.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/php.default.min.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/jquery.ddslick.min.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/detect_timezone.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/jquery.detect_timezone.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/jquery.cookie.pack.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/moment.min.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/moment-timezone-with-data-2010-2020.min.js"></script>
<script type="text/javascript" language="javascript" src="/resources/js/impromptu/jquery-impromptu.min.js"></script>

<script type="text/javascript">
$(document).ready(function() { 
	var tz_name = $(this).get_timezone();
	var tz_offset = $(this).get_offset();
	$.cookie('oitimezone', [tz_name, tz_offset], { path: '/', secure: true });
});

</script>