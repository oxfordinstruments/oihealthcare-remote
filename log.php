<?php
//Logging functions

class logger{

	public function logmsg($txt,$num = ''){
		$this->logdata($txt,1,$num);	
	}
	
	public function loginfo($txt,$num = ''){
		$this->logdata($txt,2,$num);	
	}
	
	public function logerr($txt,$num = ''){
		$this->logdata($txt,3,$num);	
	}
	
	//
	//log function
	//
	private function logdata($txt,$type,$num = ""){
		//$type = 1 message
		//$type = 2 information
		//$type = 3 error
		$protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https')	=== FALSE ? 'http' : 'https';
		$host = $_SERVER['HTTP_HOST'];
		$script = $_SERVER['SCRIPT_NAME'];
		$params = $_SERVER['QUERY_STRING'];
		$currentUrl = $protocol . '://' . $host . $script;
		if($params != ''){	
			$currentUrl .= '?'.$params;
		}
		
		$error = "[ ".date('r',time())." ] ".$currentUrl." | MSG: ".$txt;
		if($num != ""){
			$error .= " | ERR: ".$num;
		}
		if($_SESSION['login'] != ""){
			$error .= " | UID: ".$_SESSION['login'];
		}
		$error .= PHP_EOL;
		switch($type){
			case 1:
				$path = $_SERVER['DOCUMENT_ROOT']."/log/messagelog.txt";
				break;
			case 2:
				$path = $_SERVER['DOCUMENT_ROOT']."/log/infolog.txt";
				break;
			case 3:
				$path = $_SERVER['DOCUMENT_ROOT']."/log/errorlog.txt";
				break;	
		}
		error_log($error,3,$path);
	}


	//
	//Log Errors
	//
	public function logldap($txt,$num = ""){
		$protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https')	=== FALSE ? 'http' : 'https';
		$host = $_SERVER['HTTP_HOST'];
		$script = $_SERVER['SCRIPT_NAME'];
		$params = $_SERVER['QUERY_STRING'];
		$currentUrl = $protocol . '://' . $host . $script;
		if($params != ''){	
			$currentUrl .= '?'.$params;
		}
		
		$error = "[ ".date('r',time())." ] ".$currentUrl." | MSG: ".$txt;
		if($num != ""){
			$error .= " | ERR: ".$num;
		}
		if($_SESSION['login'] != ""){
			$error .= " | UID: ".$_SESSION['login'];
		}
		$error .= PHP_EOL;
		$path = $_SERVER['DOCUMENT_ROOT']."/log/ldaplog.txt";
		error_log($error,3,$path);
	}

}//end class
?>