<div id="CMSHeaderDiv"> 
    <!-- leave blank --> 
</div>
<div id="tiplayer" style="position:absolute; visibility:hidden; z-index:10000;"></div>
<header class="outerContainer" id="header">
    <div class="innerContainer" id="headerBottom">
        <div class="centerContainer container_12">
		<div class="section">
                <div class="grid_9">
				&nbsp;
                </div>
            </div>
        
            <div class="section withPadding">
                <div class="grid_3"> <a class="image" target="new" id="headerLogo" href="http://www.oxford-instruments.com/"><img src="/resources/images/common/oxford-instruments-logo.png" /></a> </div>
                <div class="grid_9">
					<nav id="mainNav" class="cf">
					<ul class="hztlNavList">
					<li class="hztlNavItem hztlNavParent"> <a class="hztlNavElem level2" href="https://oihealthcareremote.com"> <span class="hztlNavText"><span>Login</span></span> </a>
						<div class="megaNav noGridSpacing">
							<div class="megaNavInner cf">
								<div class="concertinaNav">
									<div class="grid_4">
										<div class="concertinaNavItems concertinaNavTopLevel concertinaNavItemsActive" style="background-color:inherit"> 
										</div>
									</div>
									<div class="grid_8">
										<div class="sliderViewport">
											<div class="sliderPanel">
												<div class="sliderItem sliderItemDefault"> <a href="https://oihealthcareremote.com"><img class="imageMedium" src="/resources/images/login.jpg"  /></a>
													<h2 class="level3"> Remote Diagnostics</h2>
													<p class="intro"> Login for <?php readfile($_SERVER['DOCUMENT_ROOT'].'/company_name.txt'); ?> Remote Diagnostics.</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</li>
					</nav>
                </div> 
            </div>
            <div class="section">
                <div class="grid_9">
                    <h1><a href="" style="text-decoration:none; color:#FFF"><?php readfile($_SERVER['DOCUMENT_ROOT'].'/company_name.txt'); ?> Remote Diagnostics</a></h1>
                    <p id="breadcrumb" class="breadcrumb"></p>
                </div>
            </div>
        </div>
    </div>
</header>

