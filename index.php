<!DOCTYPE html>
<html>
<head>
<title><?php readfile($_SERVER['DOCUMENT_ROOT'].'/company_name.txt'); ?> Remote</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link href="/resources/css/screen.min.css" rel="stylesheet" type="text/css" media="screen" />
<link href="/resources/css/320.min.css" rel="stylesheet" type="text/css" />
<link href="/resources/css/720.min.css" rel="stylesheet" type="text/css" />
<link href="/resources/css/960.min.css" rel="stylesheet" type="text/css" />
<link href="/resources/css/print.min.css" rel="stylesheet" type="text/css" media="print" />
<!--[if lt IE 9 ]>
        <link href="/resources/css/screen-ie.min.css" rel="stylesheet" type="text/css" />
    <![endif]-->
<!--[if IE 9 ]>
        <link href="/resources/css/screen-ie9.min.css" rel="stylesheet" media="screen" />
    <![endif]-->

<meta charset="UTF-8" />
<link href="/resources/images/favicon.ico" type="image/x-icon" rel="shortcut icon"/>
<link href="/resources/images/favicon.ico" type="image/x-icon" rel="icon"/>
<link href="resources/css/main_engineer_960.css" rel="stylesheet" type="text/css">
<link href="resources/css/main_engineer_720.css" rel="stylesheet" type="text/css">
<link href="resources/css/main_engineer_320.css" rel="stylesheet" type="text/css">
<script src="/resources/js/jquery-1.7.2.min.js"></script>
<script src="/resources/js/jquery.badBrowser.js"></script>
<xsl:variable name="companyName" select="'Test Company'"/>
</head>
<body>
<?php require($_SERVER['DOCUMENT_ROOT'].'/default_header.php'); ?>
<div id="OIReportContent"> 
	<div style="padding-top:20px; height:250px; text-align:justify; font-size:16px">
		<img src="/resources/images/CT_MR_Side_by_site_Shadow.png" width="341" height="206" style="padding-left:20px;" align="right">
		<br>
		<br>
		<p>Notice to Users:  This system is for authorized employees of <?php readfile($_SERVER['DOCUMENT_ROOT'].'/company_name.txt'); ?> only.</p>
		<p>These systems and equipment are subject to monitoring to ensure proper functioning and to protect against unauthorized access.
	Unauthorized or improper use of this system may result in administrative disciplinary action and civil and criminal penalties. </p>
	</div>
</div>
<?php require($_SERVER['DOCUMENT_ROOT'].'/default_footer.php'); ?>